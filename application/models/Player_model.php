<?php
class Player_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function insert($nickname, $facebookId)
    {
        if (!empty($facebookId)) {
            $existingPlayer = $this->getByFacebookId($facebookId);
        }

        if (empty($existingPlayer))
        {
            $this->db->insert('player', array('NICKNAME' => $nickname, 'FACEBOOK_ID' => $facebookId, 'GUEST' => empty($facebookId)));
            $id = $this->db->insert_id();

            return $this->getById($id);
        }
        else {
            return $existingPlayer;
        }
    }

    public function getById($id)
    {
        $query = $this->db->get_where('player', array('ID' => $id));
        return $query->result();
    }

    public function getByFacebookId($facebookId)
    {
        $query = $this->db->get_where('player', array('FACEBOOK_ID' => $facebookId));
        return $query->result();
    }
	
}