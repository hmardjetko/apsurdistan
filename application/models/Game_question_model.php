<?php
class Game_question_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

    public function insert($questions, $gameId)
    {
        foreach ($questions as $question)
        {
            $this->db->insert('game_question', array('ID_GAME' => $gameId, 'ID_QUESTION' => $question->ID));
        }
    }

}