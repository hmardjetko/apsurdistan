<?php
class Answer_model extends CI_Model {

	public function __construct()
	{
        $this->load->database();
	}

	public function getAll()
    {
        $query = $this->db->get('answer');
        return $query->result();
    }

    public function getById($id)
    {
        $query = $this->db->get_where('answer', array('ID' => $id));
        return $query->result();
    }

    public function getRandom($gameId)
    {
        $query = $this->db->query('SELECT ID, CONTENT FROM answer WHERE ID NOT IN (SELECT ID_ANSWER FROM game_answer WHERE ID_GAME =' . $gameId . ')');
        return $query->result();
    }
	
}