<?php
class Question_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

    public function getAll()
    {
        $query = $this->db->get('question');
        return $query->result();
    }

    public function getById($id)
    {
        $query = $this->db->get_where('question', array('ID' => $id));
        return $query->result();
    }

    public function getRandom($gameId)
    {
        $query = $this->db->query('SELECT ID, CONTENT FROM question WHERE ID NOT IN (SELECT ID_QUESTION FROM game_question WHERE ID_GAME =' . $gameId . ')');
        return $query->result();
    }
	
}