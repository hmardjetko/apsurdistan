<?php
class Game_player_model extends CI_Model {

    private static $MAX_PLAYERS = 5;

	public function __construct()
	{
		$this->load->database();
	}

	public function insert($gameId, $playerId)
    {
        return $this->db->insert('game_player', array('ID_GAME' => $gameId, 'ID_PLAYER' => $playerId));
    }

    public function addPlayer($gameId, $playerId)
    {
        if ($this->isGameFull($gameId))
        {
            return false;
        }
        else {
            return $this->insert($gameId, $playerId);
        }
    }

    public function addPlayerToAvailableGame($playerId)
    {
        $this->db->query('INSERT INTO game_player SELECT (SELECT MAX(ID) + 1 FROM game_player), ID_GAME, ' . $playerId . ' FROM game_player WHERE ID_GAME NOT IN (SELECT ID_GAME FROM game_player WHERE ID_PLAYER = ' . $playerId . ') GROUP BY ID_GAME HAVING COUNT(ID_PLAYER) < 5 LIMIT 1');
        return $this->db->affected_rows() > 0;
    }

    public function getPlayerLastGameId($playerId)
    {
        $query = $this->db->order_by('ID', 'DESC')->limit(1)->get_where('game_player', array('ID_PLAYER' => $playerId));
        return $query->row()->ID;
    }

    public function isPlayerInGame($playerId, $gameId) {
        $this->db->get_where('game_player', array('ID_GAME' => $gameId, 'ID_PLAYER' => $playerId))->row();
        return $this->db->affected_rows() > 0;
    }

    private function isGameFull($gameId)
    {
        $this->db->get_where('game_player', array('ID_GAME' => $gameId));
        return $this->db->count_all_results() >= $this::$MAX_PLAYERS;
    }

}