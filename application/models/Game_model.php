<?php
class Game_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
        $this->load->model('game_player_model');
	}

	public function create($playerId, $gameCode = null)
    {
        if (empty($gameCode)) {
            $date = new DateTime();
            $code = base64_encode($playerId . $date->getTimestamp());
        }
        else {
            $code = $gameCode;
        }

        $this->db->insert('game', array('CODE' => $code, 'ID_OWNER' => $playerId));

        $gameId = $this->db->insert_id();
        $this->addPlayer($gameId, $playerId);

        return $code;
    }

    public function addPlayer($gameId, $playerId)
    {
        return $this->game_player_model->addPlayer($gameId, $playerId);
    }

    public function addToAvailableGame($playerId)
    {
        if(empty($this->game_player_model->addPlayerToAvailableGame($playerId))) {
            $this->create($playerId);
        }
        $gameId = $this->game_player_model->getPlayerLastGameId($playerId);
        $query = $this->db->get_where('game', array('ID' => $gameId));
        return $query->row();
    }

    public function getByGameCode($gameCode)
    {
        $query = $this->db->get_where('game', array('CODE' => $gameCode));
        return $query->row();
    }

    public function isPlayerInGame($playerId, $gameId) {
        return $this->game_player_model->isPlayerInGame($playerId, $gameId);
    }

}