<?php
class Game_answer_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function insert($answers, $gameId)
    {
        foreach ($answers as $answer)
        {
            $this->db->insert('game_answer', array('ID_GAME' => $gameId, 'ID_ANSWER' => $answer->ID));
        }
    }
	
}