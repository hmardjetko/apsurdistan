<?php
require_once(APPPATH.'libraries/REST_Controller.php');
require_once(APPPATH.'libraries/Format.php');
 
class Player extends REST_Controller {
	
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('player_model');
	}

    /**
     * Creates a new player, if it isn't an existing user (check by Facebook ID) and returns it's data
     *
     * Required params: nickname
     * Optional params: facebookId
     */
	function create_post()
	{
        $nickname = $this->post('nickname');
        if (empty($nickname))
        {
            return $this->response(array('error' => 'Missing parameter: nickname'), 400);
        }

        $facebookId = $this->post('facebookId');

        $data = $this->player_model->insert($nickname, $facebookId);
        $this->response(array('player' => $data));
	}

}