<?php
require_once(APPPATH.'libraries/REST_Controller.php');
require_once(APPPATH.'libraries/Format.php');
 
class Game extends REST_Controller {
	
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('game_model');
        $this->load->model('player_model');
	}

    /**
     * Creates a new game and returns it's unique code.
     * Unique code can be sent as optional param.
     * After creating a game, automatically adds creating player to it.
     *
     * Required params: playerId
     * Optional params: gameCode
     */
	function create_post()
	{
		$playerId = $this->post('playerId');
		if (empty($playerId))
        {
            return $this->response(array('error' => 'Missing parameter: playerId'), 400);
        }

        if (!is_numeric($playerId))
        {
            return $this->response(array('error' => 'Invalid parameter: playerId. Parameter value is not a number.'), 400);
        }

        $gameCode = $this->post('gameCode');
        $game = $this->game_model->getByGameCode($gameCode);
        if (!empty($game))
        {
            return $this->response(array('error' => "Invalid game code. Game with that code already exist."), 400);
        }

        $gameCode = $this->game_model->create($playerId, $this->post('gameCode'));
		$this->response(array('gameCode' => $gameCode));
	}

    /**
     * Adds a player to specific existing game identified by gameCode param if it isn't already full.
     * If gameCode param isn't set, adds the player to the first available game or creates a new one.
     * Returns status OK or NOK.
     *
     * Required params: playerId
     * Optional params: gameCode
     */
	function addPlayer_post()
	{
	    $playerId = $this->post('playerId');
        if (empty($playerId))
        {
            return $this->response(array('error' => 'Missing parameter: playerId'), 400);
        }

        if (!is_numeric($playerId))
        {
            return $this->response(array('error' => 'Invalid parameter: playerId. Parameter value is not a number.'), 400);
        }

        $player = $this->player_model->getById($playerId);
        if (empty($player))
        {
            return $this->response(array('error' => "Invalid parameter: playerId. Player with id: " . $playerId . " doesn't exist."), 400);
        }

        $gameCode = $this->post('gameCode');
        if (!empty($gameCode))
        {
            $game = $this->game_model->getByGameCode($gameCode);
            if (empty($game))
            {
                return $this->response(array('error' => "Invalid game code. Game with code: " . $gameCode . " doesn't exist."), 400);
            }

            if ($this->game_model->isPlayerInGame($playerId, $game->ID)) {
                return $this->response(array('error' => "Player with id: " . $playerId . " is already in game with code: " . $gameCode), 400);
            }

            $gameId = $game->ID;
            $playerAdded = $this->game_model->addPlayer($gameId, $playerId);
        }
        else {
            $game = $this->game_model->addToAvailableGame($playerId);
            $playerAdded = true;
        }

        if ($playerAdded)
        {
            return $this->response(array('status' => 'OK', 'message' => 'Player added to game.', 'gameCode' => $game->CODE));
        }
        else {
            return $this->response(array('status' => 'NOK', 'message' => 'Game is full'));
        }
	}
	
}