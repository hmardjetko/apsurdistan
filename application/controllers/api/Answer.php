<?php
require_once(APPPATH.'libraries/REST_Controller.php');
require_once(APPPATH.'libraries/Format.php');
 
class Answer extends REST_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('answer_model');
        $this->load->model('game_model');
		$this->load->model('game_answer_model');
	}

    /**
     * Returns a list of all answers or filtered by id.
     *
     * Optional params: answerId
     */
	public function list_get()
	{
		$id = $this->get('answerId');
		if (!is_numeric($id)) {
			$data = $this->answer_model->getAll();
		}
		else {
			$data = $this->answer_model->getById($id);
		}

		$this->response(array('answers' => $data));
	}

    /**
     * Returns a random list of answers not already given to specific game.
     *
     * Required params: gameCode
     * Optional params: amount (default = 1)
     */
	public function random_get()
	{
	    $amount = $this->get('amount');
	    if (empty($amount) || !is_numeric($amount))
	    {
            $amount = 1;
        }

        $gameCode = $this->get('gameCode');
	    if (empty($gameCode))
	    {
	        return $this->response(array('error' => 'Missing parameter: gameCode'), 400);
        }

        $game = $this->game_model->getByGameCode($gameCode);
	    if (empty($game))
        {
            return $this->response(array('error' => "Invalid game code. Game doesn't exist."), 400);
        }

        $gameId = $game->ID;
        $data = $this->answer_model->getRandom($gameId);
	    shuffle($data);
        $data = array_splice($data, 0 , $amount);

        $this->game_answer_model->insert($data, $gameId);
        $this->response(array('answers' => $data));
    }

}