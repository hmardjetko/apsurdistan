-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for apsurdistan
CREATE DATABASE IF NOT EXISTS `apsurdistan` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `apsurdistan`;

-- Data exporting was unselected.
-- Dumping structure for table apsurdistan.player
CREATE TABLE IF NOT EXISTS `player` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NICKNAME` varchar(50) CHARACTER SET latin1 NOT NULL,
  `FACEBOOK_ID` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `GUEST` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table apsurdistan.question
CREATE TABLE IF NOT EXISTS `question` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CONTENT` varchar(1000) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping structure for table apsurdistan.answer
CREATE TABLE IF NOT EXISTS `answer` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CONTENT` varchar(1000) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table apsurdistan.game
CREATE TABLE IF NOT EXISTS `game` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CODE` varchar(100) NOT NULL,
  `ID_OWNER` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_game_player` (`ID_OWNER`),
  CONSTRAINT `FK_game_player` FOREIGN KEY (`ID_OWNER`) REFERENCES `player` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table apsurdistan.game_answer
CREATE TABLE IF NOT EXISTS `game_answer` (
  `ID_GAME` int(10) unsigned NOT NULL,
  `ID_ANSWER` int(10) unsigned NOT NULL,
  UNIQUE KEY `ID_GAME_ID_ANSWER` (`ID_GAME`,`ID_ANSWER`),
  KEY `FK_game_answer_answer` (`ID_ANSWER`),
  CONSTRAINT `FK_game_answer_answer` FOREIGN KEY (`ID_ANSWER`) REFERENCES `answer` (`ID`),
  CONSTRAINT `FK_game_answer_game` FOREIGN KEY (`ID_GAME`) REFERENCES `game` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table apsurdistan.game_player
CREATE TABLE IF NOT EXISTS `game_player` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_GAME` int(10) unsigned NOT NULL,
  `ID_PLAYER` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_GAME_ID_PLAYER` (`ID_GAME`,`ID_PLAYER`),
  KEY `FK_game_player_player` (`ID_PLAYER`),
  CONSTRAINT `FK_game_player_game` FOREIGN KEY (`ID_GAME`) REFERENCES `game` (`ID`),
  CONSTRAINT `FK_game_player_player` FOREIGN KEY (`ID_PLAYER`) REFERENCES `player` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table apsurdistan.game_question
CREATE TABLE IF NOT EXISTS `game_question` (
  `ID_GAME` int(11) unsigned NOT NULL,
  `ID_QUESTION` int(11) unsigned NOT NULL,
  UNIQUE KEY `ID_GAME_ID_QUESTION` (`ID_GAME`,`ID_QUESTION`),
  KEY `FK_game_question_question` (`ID_QUESTION`),
  CONSTRAINT `FK_game_question_game` FOREIGN KEY (`ID_GAME`) REFERENCES `game` (`ID`),
  CONSTRAINT `FK_game_question_question` FOREIGN KEY (`ID_QUESTION`) REFERENCES `question` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
